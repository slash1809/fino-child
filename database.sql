-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июн 24 2020 г., 19:44
-- Версия сервера: 5.7.30-33-log
-- Версия PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `host1700994_fino`
--

-- --------------------------------------------------------

--
-- Структура таблицы `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Автор комментария', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-06-21 17:13:51', '2020-06-21 14:13:51', 'Привет! Это комментарий.\nЧтобы начать модерировать, редактировать и удалять комментарии, перейдите на экран «Комментарии» в консоли.\nАватары авторов комментариев загружаются с сервиса <a href=\"https://ru.gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://fino.dqcore.ru', 'yes'),
(2, 'home', 'http://fino.dqcore.ru', 'yes'),
(3, 'blogname', 'FINO', 'yes'),
(4, 'blogdescription', 'Ещё один сайт на WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'gajver1809@mail.ru', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'd.m.Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:144:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"releases/?$\";s:28:\"index.php?post_type=releases\";s:41:\"releases/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=releases&feed=$matches[1]\";s:36:\"releases/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=releases&feed=$matches[1]\";s:28:\"releases/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=releases&paged=$matches[1]\";s:9:\"tracks/?$\";s:26:\"index.php?post_type=tracks\";s:39:\"tracks/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=tracks&feed=$matches[1]\";s:34:\"tracks/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=tracks&feed=$matches[1]\";s:26:\"tracks/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=tracks&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"releases/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"releases/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"releases/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"releases/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"releases/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"releases/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"releases/([^/]+)/embed/?$\";s:41:\"index.php?releases=$matches[1]&embed=true\";s:29:\"releases/([^/]+)/trackback/?$\";s:35:\"index.php?releases=$matches[1]&tb=1\";s:49:\"releases/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?releases=$matches[1]&feed=$matches[2]\";s:44:\"releases/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?releases=$matches[1]&feed=$matches[2]\";s:37:\"releases/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?releases=$matches[1]&paged=$matches[2]\";s:44:\"releases/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?releases=$matches[1]&cpage=$matches[2]\";s:33:\"releases/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?releases=$matches[1]&page=$matches[2]\";s:25:\"releases/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"releases/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"releases/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"releases/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"releases/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"releases/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"tracks/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"tracks/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"tracks/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"tracks/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"tracks/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"tracks/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"tracks/([^/]+)/embed/?$\";s:39:\"index.php?tracks=$matches[1]&embed=true\";s:27:\"tracks/([^/]+)/trackback/?$\";s:33:\"index.php?tracks=$matches[1]&tb=1\";s:47:\"tracks/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?tracks=$matches[1]&feed=$matches[2]\";s:42:\"tracks/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?tracks=$matches[1]&feed=$matches[2]\";s:35:\"tracks/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?tracks=$matches[1]&paged=$matches[2]\";s:42:\"tracks/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?tracks=$matches[1]&cpage=$matches[2]\";s:31:\"tracks/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?tracks=$matches[1]&page=$matches[2]\";s:23:\"tracks/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"tracks/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"tracks/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"tracks/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"tracks/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"tracks/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"tax_releases/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?tax_releases=$matches[1]&feed=$matches[2]\";s:48:\"tax_releases/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?tax_releases=$matches[1]&feed=$matches[2]\";s:29:\"tax_releases/([^/]+)/embed/?$\";s:45:\"index.php?tax_releases=$matches[1]&embed=true\";s:41:\"tax_releases/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?tax_releases=$matches[1]&paged=$matches[2]\";s:23:\"tax_releases/([^/]+)/?$\";s:34:\"index.php?tax_releases=$matches[1]\";s:51:\"tax_tracks/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?tax_tracks=$matches[1]&feed=$matches[2]\";s:46:\"tax_tracks/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?tax_tracks=$matches[1]&feed=$matches[2]\";s:27:\"tax_tracks/([^/]+)/embed/?$\";s:43:\"index.php?tax_tracks=$matches[1]&embed=true\";s:39:\"tax_tracks/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?tax_tracks=$matches[1]&paged=$matches[2]\";s:21:\"tax_tracks/([^/]+)/?$\";s:32:\"index.php?tax_tracks=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:33:\"wp-user-avatar/wp-user-avatar.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '3', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'fino', 'yes'),
(41, 'stylesheet', 'fino-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1608300830', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'ru_RU', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:12:\"blog-sidebar\";a:4:{i:0;s:24:\"filter_releases_widget-2\";i:1;s:8:\"search-2\";i:2;s:14:\"recent-posts-2\";i:3;s:17:\"recent-comments-2\";}s:25:\"fino-footer-widget-area-1\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}s:25:\"fino-footer-widget-area-2\";a:0:{}s:25:\"fino-footer-widget-area-3\";a:0:{}s:25:\"fino-footer-widget-area-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:7:{i:1593018831;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1593051231;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1593094431;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593094437;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593094438;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593440031;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:1:{s:22:\"McMVhObncdwIaJOO5C3J2B\";a:2:{s:10:\"hashed_key\";s:34:\"$P$B87zsQ4P7pv2I2kz5FdLaqC/EcfDbV/\";s:10:\"created_at\";i:1592993174;}}', 'yes'),
(116, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.4.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1593008034;s:15:\"version_checked\";s:5:\"5.4.2\";s:12:\"translations\";a:0:{}}', 'no'),
(118, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1593008035;s:7:\"checked\";a:2:{s:30:\"advanced-custom-fields/acf.php\";s:6:\"5.8.12\";s:33:\"wp-user-avatar/wp-user-avatar.php\";s:5:\"2.2.7\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"5.8.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"wp-user-avatar/wp-user-avatar.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wp-user-avatar\";s:4:\"slug\";s:14:\"wp-user-avatar\";s:6:\"plugin\";s:33:\"wp-user-avatar/wp-user-avatar.php\";s:11:\"new_version\";s:5:\"2.2.7\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-user-avatar/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wp-user-avatar.2.2.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-user-avatar/assets/icon-256x256.png?rev=1755722\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-user-avatar/assets/icon-128x128.png?rev=1755722\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/wp-user-avatar/assets/banner-772x250.png?rev=882713\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(121, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1593008035;s:7:\"checked\";a:2:{s:10:\"fino-child\";s:5:\"0.1.0\";s:4:\"fino\";s:5:\"1.1.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(122, '_site_transient_timeout_browser_0a48e95c5db00f30047be3181e9619dd', '1593353638', 'no'),
(123, '_site_transient_browser_0a48e95c5db00f30047be3181e9619dd', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"83.0.4103.106\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(124, '_site_transient_timeout_php_check_56babb1797dd31750a342dc4c8a11025', '1593353638', 'no'),
(125, '_site_transient_php_check_56babb1797dd31750a342dc4c8a11025', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(127, 'can_compress_scripts', '0', 'no'),
(140, 'theme_mods_twentytwenty', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1592748845;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(141, 'current_theme', 'Fino-Child', 'yes'),
(142, 'theme_mods_fino-child', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:7;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(143, 'theme_switched', '', 'yes'),
(148, 'tax_tracks_children', 'a:0:{}', 'yes'),
(149, 'recently_activated', 'a:0:{}', 'yes'),
(150, 'widget_wp_user_avatar_profile', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(151, 'acf_version', '5.8.12', 'yes'),
(152, 'avatar_default_wp_user_avatar', '', 'yes'),
(153, 'wp_user_avatar_allow_upload', '0', 'yes'),
(154, 'wp_user_avatar_disable_gravatar', '0', 'yes'),
(155, 'wp_user_avatar_edit_avatar', '1', 'yes'),
(156, 'wp_user_avatar_resize_crop', '0', 'yes'),
(157, 'wp_user_avatar_resize_h', '96', 'yes'),
(158, 'wp_user_avatar_resize_upload', '0', 'yes'),
(159, 'wp_user_avatar_resize_w', '96', 'yes'),
(160, 'wp_user_avatar_tinymce', '1', 'yes'),
(161, 'wp_user_avatar_upload_size_limit', '0', 'yes'),
(162, 'wp_user_avatar_default_avatar_updated', '1', 'yes'),
(163, 'wp_user_avatar_users_updated', '1', 'yes'),
(164, 'wp_user_avatar_media_updated', '1', 'yes'),
(165, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(181, '_site_transient_timeout_browser_dc533af2dc34d59fa5f9e5e34b0fe56f', '1593432348', 'no'),
(182, '_site_transient_browser_dc533af2dc34d59fa5f9e5e34b0fe56f', 'a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"77.0\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:32:\"https://www.mozilla.org/firefox/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(199, '_transient_health-check-site-status-result', '{\"good\":\"9\",\"recommended\":\"8\",\"critical\":\"0\"}', 'yes'),
(221, 'tax_releases_children', 'a:0:{}', 'yes'),
(222, 'recovery_mode_email_last_sent', '1592993174', 'yes'),
(239, 'widget_filter_releases_widget', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:18:\"acf_releases_group\";s:2:\"27\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(258, '_site_transient_timeout_theme_roots', '1593009834', 'no'),
(259, '_site_transient_theme_roots', 'a:2:{s:10:\"fino-child\";s:7:\"/themes\";s:4:\"fino\";s:7:\"/themes\";}', 'no');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 6, '_edit_lock', '1592827871:2'),
(4, 6, '_edit_last', '1'),
(5, 6, 'release', '1'),
(6, 6, '_release', 'field_5eec7bb341f5c'),
(7, 6, 'year', '20200620'),
(8, 6, '_year', 'field_5eea4e1acff58'),
(9, 6, 'time', '00:03:00'),
(10, 6, '_time', 'field_5eea4e4dcff59'),
(11, 6, 'file', ''),
(12, 6, '_file', 'field_5eea54c245608'),
(13, 7, '_edit_lock', '1592749107:1'),
(14, 7, '_edit_last', '1'),
(15, 7, 'release', '0'),
(16, 7, '_release', 'field_5eec7bb341f5c'),
(17, 7, 'year', '20060609'),
(18, 7, '_year', 'field_5eea4e1acff58'),
(19, 7, 'time', '00:02:00'),
(20, 7, '_time', 'field_5eea4e4dcff59'),
(21, 7, 'file', ''),
(22, 7, '_file', 'field_5eea54c245608'),
(23, 9, '_edit_lock', '1592912522:1'),
(24, 9, '_edit_last', '1'),
(25, 10, '_wp_attached_file', '2020/06/new_logo-2.jpg'),
(26, 10, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:600;s:4:\"file\";s:22:\"2020/06/new_logo-2.jpg\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"new_logo-2-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"new_logo-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"new_logo-2-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"fino_release_img\";a:4:{s:4:\"file\";s:22:\"new_logo-2-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"fino-page-thumbnail\";a:4:{s:4:\"file\";s:22:\"new_logo-2-738x423.jpg\";s:5:\"width\";i:738;s:6:\"height\";i:423;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"fino-photo-home\";a:4:{s:4:\"file\";s:22:\"new_logo-2-360x244.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:244;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:17:\"fino-photo-single\";a:4:{s:4:\"file\";s:22:\"new_logo-2-800x411.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:411;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"fino-photo-blog\";a:4:{s:4:\"file\";s:22:\"new_logo-2-408x244.jpg\";s:5:\"width\";i:408;s:6:\"height\";i:244;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:24:\"fino-casestudy-thumbnail\";a:4:{s:4:\"file\";s:22:\"new_logo-2-555x286.jpg\";s:5:\"width\";i:555;s:6:\"height\";i:286;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"fino_client_img\";a:4:{s:4:\"file\";s:22:\"new_logo-2-170x120.jpg\";s:5:\"width\";i:170;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(27, 11, '_wp_attached_file', '2020/06/unnamed-2.jpg'),
(28, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:383;s:6:\"height\";i:383;s:4:\"file\";s:21:\"2020/06/unnamed-2.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"unnamed-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"unnamed-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"fino_release_img\";a:4:{s:4:\"file\";s:21:\"unnamed-2-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"fino-photo-home\";a:4:{s:4:\"file\";s:21:\"unnamed-2-360x244.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:244;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"fino-photo-blog\";a:4:{s:4:\"file\";s:21:\"unnamed-2-383x244.jpg\";s:5:\"width\";i:383;s:6:\"height\";i:244;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:24:\"fino-casestudy-thumbnail\";a:4:{s:4:\"file\";s:21:\"unnamed-2-383x286.jpg\";s:5:\"width\";i:383;s:6:\"height\";i:286;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"fino_client_img\";a:4:{s:4:\"file\";s:21:\"unnamed-2-170x120.jpg\";s:5:\"width\";i:170;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 9, '_thumbnail_id', '10'),
(30, 9, 'genre', 'a:1:{i:0;s:3:\"pop\";}'),
(31, 9, '_genre', 'field_5ee8e71d4c7b8'),
(32, 9, 'style', 'a:2:{i:0;s:8:\"pop_rock\";i:1;s:8:\"synthpop\";}'),
(33, 9, '_style', 'field_5ee8e7e04c7b9'),
(34, 9, 'format', 'a:1:{i:0;s:1:\"2\";}'),
(35, 9, '_format', 'field_5ee8e8134c7ba'),
(36, 9, 'country', 'USA'),
(37, 9, '_country', 'field_5ee8e82d4c7bb'),
(38, 9, 'release_date', '20200620'),
(39, 9, '_release_date', 'field_5ee8e9bc4c7bc'),
(40, 9, 'worked_on_release', ''),
(41, 9, '_worked_on_release', 'field_5ee90b6a6b69e'),
(42, 1, '_wp_trash_meta_status', 'publish'),
(43, 1, '_wp_trash_meta_time', '1592749350'),
(44, 1, '_wp_desired_post_slug', '%d0%bf%d1%80%d0%b8%d0%b2%d0%b5%d1%82-%d0%bc%d0%b8%d1%80'),
(45, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(46, 13, '_menu_item_type', 'custom'),
(47, 13, '_menu_item_menu_item_parent', '0'),
(48, 13, '_menu_item_object_id', '13'),
(49, 13, '_menu_item_object', 'custom'),
(50, 13, '_menu_item_target', ''),
(51, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(52, 13, '_menu_item_xfn', ''),
(53, 13, '_menu_item_url', 'http://fino.dqcore.ru/'),
(55, 14, '_menu_item_type', 'post_type'),
(56, 14, '_menu_item_menu_item_parent', '0'),
(57, 14, '_menu_item_object_id', '5'),
(58, 14, '_menu_item_object', 'page'),
(59, 14, '_menu_item_target', ''),
(60, 14, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(61, 14, '_menu_item_xfn', ''),
(62, 14, '_menu_item_url', ''),
(63, 14, '_menu_item_orphaned', '1592749356'),
(64, 15, '_menu_item_type', 'post_type'),
(65, 15, '_menu_item_menu_item_parent', '0'),
(66, 15, '_menu_item_object_id', '2'),
(67, 15, '_menu_item_object', 'page'),
(68, 15, '_menu_item_target', ''),
(69, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(70, 15, '_menu_item_xfn', ''),
(71, 15, '_menu_item_url', ''),
(72, 15, '_menu_item_orphaned', '1592749356'),
(73, 17, '_edit_lock', '1592920139:3'),
(74, 17, '_edit_last', '3'),
(75, 17, '_thumbnail_id', '10'),
(76, 17, 'genre', 'a:1:{i:0;s:3:\"pop\";}'),
(77, 17, '_genre', 'field_5ee8e71d4c7b8'),
(78, 17, 'style', 'a:2:{i:0;s:8:\"pop_rock\";i:1;s:8:\"synthpop\";}'),
(79, 17, '_style', 'field_5ee8e7e04c7b9'),
(80, 17, 'format', 'a:2:{i:0;s:1:\"2\";i:1;s:1:\"6\";}'),
(81, 17, '_format', 'field_5ee8e8134c7ba'),
(82, 17, 'country', 'USA'),
(83, 17, '_country', 'field_5ee8e82d4c7bb'),
(84, 17, 'release_date', '20160610'),
(85, 17, '_release_date', 'field_5ee8e9bc4c7bc'),
(86, 17, 'worked_on_release', 'a:1:{i:0;s:1:\"1\";}'),
(87, 17, '_worked_on_release', 'field_5ee90b6a6b69e'),
(89, 11, '_wp_attachment_wp_user_avatar', '3'),
(90, 18, '_edit_lock', '1592749580:3'),
(91, 18, '_edit_last', '3'),
(92, 18, 'release', '1'),
(93, 18, '_release', 'field_5eec7bb341f5c'),
(94, 18, 'year', '20200612'),
(95, 18, '_year', 'field_5eea4e1acff58'),
(96, 18, 'time', '00:05:00'),
(97, 18, '_time', 'field_5eea4e4dcff59'),
(98, 18, 'file', ''),
(99, 18, '_file', 'field_5eea54c245608'),
(100, 19, '_edit_lock', '1592749605:3'),
(101, 19, '_edit_last', '3'),
(102, 19, 'release', '1'),
(103, 19, '_release', 'field_5eec7bb341f5c'),
(104, 19, 'year', '20040611'),
(105, 19, '_year', 'field_5eea4e1acff58'),
(106, 19, 'time', '00:05:00'),
(107, 19, '_time', 'field_5eea4e4dcff59'),
(108, 19, 'file', ''),
(109, 19, '_file', 'field_5eea54c245608'),
(110, 20, '_edit_last', '3'),
(111, 20, 'release', '1'),
(112, 20, '_release', 'field_5eec7bb341f5c'),
(113, 20, 'year', '20060623'),
(114, 20, '_year', 'field_5eea4e1acff58'),
(115, 20, 'time', '00:04:00'),
(116, 20, '_time', 'field_5eea4e4dcff59'),
(117, 20, 'file', ''),
(118, 20, '_file', 'field_5eea54c245608'),
(119, 20, '_edit_lock', '1592908766:1'),
(120, 21, '_edit_lock', '1592909386:1'),
(121, 21, '_edit_last', '3'),
(122, 21, 'release', '1'),
(123, 21, '_release', 'field_5eec7bb341f5c'),
(124, 21, 'year', '20200605'),
(125, 21, '_year', 'field_5eea4e1acff58'),
(126, 21, 'time', '00:02:00'),
(127, 21, '_time', 'field_5eea4e4dcff59'),
(128, 21, 'file', ''),
(129, 21, '_file', 'field_5eea54c245608'),
(130, 17, 'tracks_list', 's:19:\"a:1:{i:0;s:2:\"18\";}\";'),
(131, 9, '_wp_old_date', '2020-06-21'),
(132, 17, '_wp_old_date', '2020-06-21'),
(133, 9, '_wp_old_date', '2003-06-21'),
(134, 22, '_edit_lock', '1592919704:3'),
(135, 22, '_edit_last', '3'),
(136, 22, 'genre', 'a:2:{i:0;s:4:\"rock\";i:1;s:3:\"pop\";}'),
(137, 22, '_genre', 'field_5ee8e71d4c7b8'),
(138, 22, 'style', 'a:3:{i:0;s:9:\"hard_rock\";i:1;s:8:\"pop_rock\";i:2;s:8:\"synthpop\";}'),
(139, 22, '_style', 'field_5ee8e7e04c7b9'),
(140, 22, 'format', 'a:1:{i:0;s:1:\"3\";}'),
(141, 22, '_format', 'field_5ee8e8134c7ba'),
(142, 22, 'country', 'Russia'),
(143, 22, '_country', 'field_5ee8e82d4c7bb'),
(144, 22, 'release_date', '20160616'),
(145, 22, '_release_date', 'field_5ee8e9bc4c7bc'),
(146, 22, 'worked_on_release', ''),
(147, 22, '_worked_on_release', 'field_5ee90b6a6b69e'),
(148, 22, 'tracks_list', 's:19:\"a:1:{i:0;s:2:\"18\";}\";'),
(149, 22, '_thumbnail_id', '10'),
(150, 23, '_edit_lock', '1592919675:3'),
(151, 23, '_edit_last', '3'),
(152, 23, '_thumbnail_id', '10'),
(153, 23, 'genre', 'a:1:{i:0;s:4:\"rock\";}'),
(154, 23, '_genre', 'field_5ee8e71d4c7b8'),
(155, 23, 'style', 'a:2:{i:0;s:8:\"pop_rock\";i:1;s:8:\"synthpop\";}'),
(156, 23, '_style', 'field_5ee8e7e04c7b9'),
(157, 23, 'format', 'a:3:{i:0;s:1:\"3\";i:1;s:1:\"6\";i:2;s:1:\"5\";}'),
(158, 23, '_format', 'field_5ee8e8134c7ba'),
(159, 23, 'country', 'USA'),
(160, 23, '_country', 'field_5ee8e82d4c7bb'),
(161, 23, 'release_date', '20180623'),
(162, 23, '_release_date', 'field_5ee8e9bc4c7bc'),
(163, 23, 'worked_on_release', 'a:1:{i:0;s:1:\"1\";}'),
(164, 23, '_worked_on_release', 'field_5ee90b6a6b69e'),
(165, 23, 'tracks_list', 's:32:\"a:2:{i:0;s:2:\"20\";i:1;s:2:\"19\";}\";'),
(166, 24, '_edit_lock', '1593016309:1'),
(167, 24, '_edit_last', '3'),
(168, 24, '_thumbnail_id', '10'),
(169, 24, 'genre', 'a:1:{i:0;s:3:\"pop\";}'),
(170, 24, '_genre', 'field_5ee8e71d4c7b8'),
(171, 24, 'style', 'a:1:{i:0;s:9:\"hard_rock\";}'),
(172, 24, '_style', 'field_5ee8e7e04c7b9'),
(173, 24, 'format', 'a:1:{i:0;s:1:\"3\";}'),
(174, 24, '_format', 'field_5ee8e8134c7ba'),
(175, 24, 'country', 'USA'),
(176, 24, '_country', 'field_5ee8e82d4c7bb'),
(177, 24, 'release_date', '20200612'),
(178, 24, '_release_date', 'field_5ee8e9bc4c7bc'),
(179, 24, 'worked_on_release', ''),
(180, 24, '_worked_on_release', 'field_5ee90b6a6b69e'),
(181, 24, 'tracks_list', 's:19:\"a:1:{i:0;s:2:\"21\";}\";'),
(182, 5, '_edit_lock', '1592827993:2'),
(183, 26, '_edit_lock', '1592904886:1'),
(184, 27, '_edit_lock', '1593016289:1'),
(185, 27, '_edit_last', '1'),
(186, 34, '_edit_lock', '1592909572:1'),
(187, 34, '_edit_last', '1'),
(188, 39, '_edit_lock', '1592912252:1'),
(189, 39, '_edit_last', '1'),
(190, 39, 'year', '20120621'),
(191, 39, '_year', 'field_5eea4e1acff58'),
(192, 39, 'time', '00:04:00'),
(193, 39, '_time', 'field_5eea4e4dcff59'),
(194, 39, 'file', ''),
(195, 39, '_file', 'field_5eea54c245608'),
(196, 9, 'tracks_list', 's:18:\"a:1:{i:0;s:1:\"6\";}\";');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-06-21 17:13:51', '2020-06-21 14:13:51', '<!-- wp:paragraph -->\n<p>Добро пожаловать в WordPress. Это ваша первая запись. Отредактируйте или удалите ее, затем начинайте создавать!</p>\n<!-- /wp:paragraph -->', 'Привет, мир!', '', 'trash', 'open', 'open', '', '%d0%bf%d1%80%d0%b8%d0%b2%d0%b5%d1%82-%d0%bc%d0%b8%d1%80__trashed', '', '', '2020-06-21 17:22:30', '2020-06-21 14:22:30', '', 0, 'http://fino.dqcore.ru/?p=1', 0, 'post', '', 1),
(2, 1, '2020-06-21 17:13:51', '2020-06-21 14:13:51', '<!-- wp:paragraph -->\n<p>Это пример страницы. От записей в блоге она отличается тем, что остаётся на одном месте и отображается в меню сайта (в большинстве тем). На странице &laquo;Детали&raquo; владельцы сайтов обычно рассказывают о себе потенциальным посетителям. Например, так:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Привет! Днём я курьер, а вечером &#8212; подающий надежды актёр. Это мой блог. Я живу в Ростове-на-Дону, люблю своего пса Джека и пинаколаду. (И ещё попадать под дождь.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...или так:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Компания &laquo;Штучки XYZ&raquo; была основана в 1971 году и с тех пор производит качественные штучки. Компания находится в Готэм-сити, имеет штат из более чем 2000 сотрудников и приносит много пользы жителям Готэма.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Перейдите <a href=\"http://fino.dqcore.ru/wp-admin/\">в консоль</a>, чтобы удалить эту страницу и создать новые. Успехов!</p>\n<!-- /wp:paragraph -->', 'Пример страницы', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2020-06-21 17:13:51', '2020-06-21 14:13:51', '', 0, 'http://fino.dqcore.ru/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-06-21 17:13:51', '2020-06-21 14:13:51', '<!-- wp:heading --><h2>Кто мы</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Наш адрес сайта: http://fino.dqcore.ru.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Какие персональные данные мы собираем и с какой целью</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Комментарии</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Если посетитель оставляет комментарий на сайте, мы собираем данные указанные в форме комментария, а также IP адрес посетителя и данные user-agent браузера с целью определения спама.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Анонимизированная строка создаваемая из вашего адреса email (\"хеш\") может предоставляться сервису Gravatar, чтобы определить используете ли вы его. Политика конфиденциальности Gravatar доступна здесь: https://automattic.com/privacy/ . После одобрения комментария ваше изображение профиля будет видимым публично в контексте вашего комментария.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Медиафайлы</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Если вы зарегистрированный пользователь и загружаете фотографии на сайт, вам возможно следует избегать загрузки изображений с метаданными EXIF, так как они могут содержать данные вашего месторасположения по GPS. Посетители могут извлечь эту информацию скачав изображения с сайта.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Формы контактов</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Куки</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Если вы оставляете комментарий на нашем сайте, вы можете включить сохранение вашего имени, адреса email и вебсайта в куки. Это делается для вашего удобства, чтобы не заполнять данные снова при повторном комментировании. Эти куки хранятся в течение одного года.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Если у вас есть учетная запись на сайте и вы войдете в неё, мы установим временный куки для определения поддержки куки вашим браузером, куки не содержит никакой личной информации и удаляется при закрытии вашего браузера.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>При входе в учетную запись мы также устанавливаем несколько куки с данными входа и настройками экрана. Куки входа хранятся в течение двух дней, куки с настройками экрана - год. Если вы выберете возможность \"Запомнить меня\", данные о входе будут сохраняться в течение двух недель. При выходе из учетной записи куки входа будут удалены.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>При редактировании или публикации статьи в браузере будет сохранен дополнительный куки, он не содержит персональных данных и содержит только ID записи отредактированной вами, истекает через 1 день.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Встраиваемое содержимое других вебсайтов</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Статьи на этом сайте могут включать встраиваемое содержимое (например видео, изображения, статьи и др.), подобное содержимое ведет себя так же, как если бы посетитель зашел на другой сайт.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Эти сайты могут собирать данные о вас, использовать куки, внедрять дополнительное отслеживание третьей стороной и следить за вашим взаимодействием с внедренным содержимым, включая отслеживание взаимодействия, если у вас есть учетная запись и вы авторизовались на том сайте.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Веб-аналитика</h3><!-- /wp:heading --><!-- wp:heading --><h2>С кем мы делимся вашими данными</h2><!-- /wp:heading --><!-- wp:heading --><h2>Как долго мы храним ваши данные</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Если вы оставляете комментарий, то сам комментарий и его метаданные сохраняются неопределенно долго. Это делается для того, чтобы определять и одобрять последующие комментарии автоматически, вместо помещения их в очередь на одобрение.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Для пользователей с регистрацией на нашем сайте мы храним ту личную информацию, которую они указывают в своем профиле. Все пользователи могут видеть, редактировать или удалить свою информацию из профиля в любое время (кроме имени пользователя). Администрация вебсайта также может видеть и изменять эту информацию.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Какие у вас права на ваши данные</h2><!-- /wp:heading --><!-- wp:paragraph --><p>При наличии учетной записи на сайте или если вы оставляли комментарии, то вы можете запросить файл экспорта персональных данных, которые мы сохранили о вас, включая предоставленные вами данные. Вы также можете запросить удаление этих данных, это не включает данные, которые мы обязаны хранить в административных целях, по закону или целях безопасности.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Куда мы отправляем ваши данные</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Комментарии пользователей могут проверяться автоматическим сервисом определения спама.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Ваша контактная информация</h2><!-- /wp:heading --><!-- wp:heading --><h2>Дополнительная информация</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Как мы защищаем ваши данные</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Какие принимаются процедуры против взлома данных</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>От каких третьих сторон мы получаем данные</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Какие автоматические решения принимаются на основе данных пользователей</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Требования к раскрытию отраслевых нормативных требований</h3><!-- /wp:heading -->', 'Политика конфиденциальности', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-06-21 17:13:51', '2020-06-21 14:13:51', '', 0, 'http://fino.dqcore.ru/?page_id=3', 0, 'page', '', 0),
(4, 1, '2020-06-21 17:13:58', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-06-21 17:13:58', '0000-00-00 00:00:00', '', 0, 'http://fino.dqcore.ru/?p=4', 0, 'post', '', 0),
(5, 1, '2020-06-21 17:14:05', '2020-06-21 14:14:05', '', 'artist', '', 'publish', 'closed', 'closed', '', 'artist', '', '', '2020-06-21 17:14:05', '2020-06-21 14:14:05', '', 0, 'http://fino.dqcore.ru/artist/', 0, 'page', '', 0),
(6, 1, '2020-06-21 17:20:25', '2020-06-21 14:20:25', '', 'Track #1', '', 'publish', 'closed', 'closed', '', 'track-1', '', '', '2020-06-21 17:20:25', '2020-06-21 14:20:25', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&#038;p=6', 0, 'tracks', '', 0),
(7, 1, '2020-06-21 17:20:50', '2020-06-21 14:20:50', '', 'Track #2', '', 'publish', 'closed', 'closed', '', 'track-2', '', '', '2020-06-21 17:20:50', '2020-06-21 14:20:50', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&#038;p=7', 0, 'tracks', '', 0),
(8, 1, '2020-06-21 17:20:51', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-06-21 17:20:51', '0000-00-00 00:00:00', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&p=8', 0, 'tracks', '', 0),
(9, 1, '2010-06-21 17:22:21', '2010-06-21 14:22:21', 'He felicity no an at packages answered opinions juvenile. To things so denied admire. Fortune day out married parties. Advantages entreaties mr he apartments do. Draw fond rank form nor the day eat. Up hung mr we give rest half. Strictly numerous outlived kindness whatever on we no on addition. Sentiments two occasional affronting solicitud\r\n\r\nHe felicity no an at packages answered opinions juvenile. To things so denied admire. Fortune day out married parties. Advantages entreaties mr he apartments do. Draw fond rank form nor the day eat. Up hung mr we give rest half. Strictly numerous outlived kindness whatever on we no on addition. Sentiments two occasional affronting solicitud', 'Release #1', '', 'publish', 'closed', 'closed', '', 'release-1', '', '', '2020-06-23 14:44:20', '2020-06-23 11:44:20', '', 0, 'http://fino.dqcore.ru/?post_type=releases&#038;p=9', 0, 'releases', '', 0),
(10, 1, '2020-06-21 17:21:46', '2020-06-21 14:21:46', '', 'new_logo', '', 'inherit', 'open', 'closed', '', 'new_logo', '', '', '2020-06-21 17:21:46', '2020-06-21 14:21:46', '', 9, 'http://fino.dqcore.ru/wp-content/uploads/2020/06/new_logo-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(11, 1, '2020-06-21 17:21:47', '2020-06-21 14:21:47', '', 'unnamed', '', 'inherit', 'open', 'closed', '', 'unnamed', '', '', '2020-06-21 17:21:47', '2020-06-21 14:21:47', '', 9, 'http://fino.dqcore.ru/wp-content/uploads/2020/06/unnamed-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2020-06-21 17:22:30', '2020-06-21 14:22:30', '<!-- wp:paragraph -->\n<p>Добро пожаловать в WordPress. Это ваша первая запись. Отредактируйте или удалите ее, затем начинайте создавать!</p>\n<!-- /wp:paragraph -->', 'Привет, мир!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-06-21 17:22:30', '2020-06-21 14:22:30', '', 1, 'http://fino.dqcore.ru/2020/06/21/1-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2020-06-21 17:22:59', '2020-06-21 14:22:59', '', 'Главная', '', 'publish', 'closed', 'closed', '', '%d0%b3%d0%bb%d0%b0%d0%b2%d0%bd%d0%b0%d1%8f', '', '', '2020-06-21 17:42:08', '2020-06-21 14:42:08', '', 0, 'http://fino.dqcore.ru/?p=13', 1, 'nav_menu_item', '', 0),
(14, 1, '2020-06-21 17:22:36', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-21 17:22:36', '0000-00-00 00:00:00', '', 0, 'http://fino.dqcore.ru/?p=14', 1, 'nav_menu_item', '', 0),
(15, 1, '2020-06-21 17:22:36', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-06-21 17:22:36', '0000-00-00 00:00:00', '', 0, 'http://fino.dqcore.ru/?p=15', 1, 'nav_menu_item', '', 0),
(16, 3, '2020-06-21 17:26:55', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-06-21 17:26:55', '0000-00-00 00:00:00', '', 0, 'http://fino.dqcore.ru/?p=16', 0, 'post', '', 0),
(17, 3, '2009-06-21 17:27:41', '2009-06-21 14:27:41', 'Made neat an on be gave show snug tore. Course sir people worthy horses add entire suffer. Fortune day out married parties. At principle perfectly by sweetness do. Fortune day out married parties. You high bed wish help call draw side. Any delicate you how kindness horrible outlived servants.\r\n\r\nMade neat an on be gave show snug tore. Course sir people worthy horses add entire suffer. Fortune day out married parties. At principle perfectly by sweetness do. Fortune day out married parties. You high bed wish help call draw side. Any delicate you how kindness horrible outlived servants.\r\n\r\nMade neat an on be gave show snug tore. Course sir people worthy horses add entire suffer. Fortune day out married parties. At principle perfectly by sweetness do. Fortune day out married parties. You high bed wish help call draw side. Any delicate you how kindness horrible outlived servants.', 'Release #2', '', 'publish', 'closed', 'closed', '', 'release-2', '', '', '2020-06-23 16:44:26', '2020-06-23 13:44:26', '', 0, 'http://fino.dqcore.ru/?post_type=releases&#038;p=17', 0, 'releases', '', 0),
(18, 3, '2020-06-21 17:28:43', '2020-06-21 14:28:43', '', 'Track #3', '', 'publish', 'closed', 'closed', '', 'track-3', '', '', '2020-06-21 17:28:43', '2020-06-21 14:28:43', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&#038;p=18', 0, 'tracks', '', 0),
(19, 3, '2020-06-21 17:29:08', '2020-06-21 14:29:08', '', 'Track #4', '', 'publish', 'closed', 'closed', '', 'track-4', '', '', '2020-06-21 17:29:08', '2020-06-21 14:29:08', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&#038;p=19', 0, 'tracks', '', 0),
(20, 3, '2020-06-21 17:29:27', '2020-06-21 14:29:27', '', 'Track #5', '', 'publish', 'closed', 'closed', '', '20', '', '', '2020-06-21 17:29:41', '2020-06-21 14:29:41', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&#038;p=20', 0, 'tracks', '', 0),
(21, 3, '2020-06-21 17:30:06', '2020-06-21 14:30:06', '', 'Track #6', '', 'publish', 'closed', 'closed', '', 'track-6', '', '', '2020-06-21 17:30:06', '2020-06-21 14:30:06', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&#038;p=21', 0, 'tracks', '', 0),
(22, 3, '2020-06-21 17:49:51', '2020-06-21 14:49:51', 'Made neat an on be gave show snug tore. Course sir people worthy horses add entire suffer. Fortune day out married parties. At principle perfectly by sweetness do. Fortune day out married parties. You high bed wish help call draw side. Any delicate you how kindness horrible outlived servants.', 'Release #3', '', 'publish', 'closed', 'closed', '', 'release-3', '', '', '2020-06-23 16:43:54', '2020-06-23 13:43:54', '', 0, 'http://fino.dqcore.ru/?post_type=releases&#038;p=22', 0, 'releases', '', 0),
(23, 3, '2020-06-21 17:51:52', '2020-06-21 14:51:52', 'Draw from upon here gone add one. Sitting hearted on it without me. Celebrated delightful an especially increasing instrument am. Small for ask shade water manor think men begin. Happiness remainder joy but earnestly for off. In expression an solicitude principles in do. Fat new smallness few supposing suspicion two.', 'Release #4', '', 'publish', 'closed', 'closed', '', 'release-4', '', '', '2020-06-23 16:43:38', '2020-06-23 13:43:38', '', 0, 'http://fino.dqcore.ru/?post_type=releases&#038;p=23', 0, 'releases', '', 0),
(24, 3, '2020-06-21 18:25:13', '2020-06-21 15:25:13', 'Took sold add play may none him few. Painful so he an comfort is manners. Happiness remainder joy but earnestly for off. Made neat an on be gave show snug tore. Their saved linen downs tears son add music. Her too add narrow having wished. We me rent been part what.', 'Release #5', '', 'publish', 'closed', 'closed', '', 'release-5', '', '', '2020-06-23 16:43:25', '2020-06-23 13:43:25', '', 0, 'http://fino.dqcore.ru/?post_type=releases&#038;p=24', 0, 'releases', '', 0),
(25, 2, '2020-06-22 15:05:48', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-06-22 15:05:48', '0000-00-00 00:00:00', '', 0, 'http://fino.dqcore.ru/?p=25', 0, 'post', '', 0),
(26, 1, '2020-06-23 12:34:46', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-06-23 12:34:46', '0000-00-00 00:00:00', '', 0, 'http://fino.dqcore.ru/?post_type=acf-field-group&p=26', 0, 'acf-field-group', '', 0),
(27, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"releases\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Fino releases', 'fino-releases', 'publish', 'closed', 'closed', '', 'group_5ee8e71247012', '', '', '2020-06-24 15:17:43', '2020-06-24 12:17:43', '', 0, 'http://fino.dqcore.ru/?p=27', 0, 'acf-field-group', '', 0),
(28, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:3:{s:4:\"rock\";s:4:\"Rock\";s:3:\"pop\";s:3:\"Pop\";s:10:\"electronic\";s:10:\"Electronic\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"label\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Genre', 'genre', 'publish', 'closed', 'closed', '', 'field_5ee8e71d4c7b8', '', '', '2020-06-23 12:35:04', '2020-06-23 09:35:04', '', 27, 'http://fino.dqcore.ru/?post_type=acf-field&p=28', 0, 'acf-field', '', 0),
(29, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:3:{s:9:\"hard_rock\";s:9:\"Hard Rock\";s:8:\"pop_rock\";s:8:\"Pop Rock\";s:8:\"synthpop\";s:8:\"Synthpop\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"label\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Style', 'style', 'publish', 'closed', 'closed', '', 'field_5ee8e7e04c7b9', '', '', '2020-06-23 12:35:04', '2020-06-23 09:35:04', '', 27, 'http://fino.dqcore.ru/?post_type=acf-field&p=29', 1, 'acf-field', '', 0),
(31, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Country', 'country', 'publish', 'closed', 'closed', '', 'field_5ee8e82d4c7bb', '', '', '2020-06-23 12:38:20', '2020-06-23 09:38:20', '', 27, 'http://fino.dqcore.ru/?post_type=acf-field&#038;p=31', 2, 'acf-field', '', 0),
(32, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:1:\"Y\";s:13:\"return_format\";s:1:\"Y\";s:9:\"first_day\";i:1;}', 'Decade', 'release_date', 'publish', 'closed', 'closed', '', 'field_5ee8e9bc4c7bc', '', '', '2020-06-24 15:17:43', '2020-06-24 12:17:43', '', 27, 'http://fino.dqcore.ru/?post_type=acf-field&#038;p=32', 3, 'acf-field', '', 0),
(33, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:9:{s:4:\"type\";s:4:\"user\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"role\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:13:\"return_format\";s:5:\"array\";}', 'Worked on the release', 'worked_on_release', 'publish', 'closed', 'closed', '', 'field_5ee90b6a6b69e', '', '', '2020-06-23 12:38:20', '2020-06-23 09:38:20', '', 27, 'http://fino.dqcore.ru/?post_type=acf-field&#038;p=33', 4, 'acf-field', '', 0),
(34, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"tracks\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:13:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"author\";i:8;s:6:\"format\";i:9;s:15:\"page_attributes\";i:10;s:10:\"categories\";i:11;s:4:\"tags\";i:12;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Fino tracks', 'fino-tracks', 'publish', 'closed', 'closed', '', 'group_5eea4de956423', '', '', '2020-06-23 13:54:48', '2020-06-23 10:54:48', '', 0, 'http://fino.dqcore.ru/?p=34', 0, 'acf-field-group', '', 0),
(36, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:1:\"Y\";s:13:\"return_format\";s:1:\"Y\";s:9:\"first_day\";i:1;}', 'Year', 'year', 'publish', 'closed', 'closed', '', 'field_5eea4e1acff58', '', '', '2020-06-23 13:46:27', '2020-06-23 10:46:27', '', 34, 'http://fino.dqcore.ru/?post_type=acf-field&#038;p=36', 0, 'acf-field', '', 0),
(37, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:7:{s:4:\"type\";s:11:\"time_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"H:i:s\";s:13:\"return_format\";s:5:\"H:i:s\";}', 'Time', 'time', 'publish', 'closed', 'closed', '', 'field_5eea4e4dcff59', '', '', '2020-06-23 13:46:27', '2020-06-23 10:46:27', '', 34, 'http://fino.dqcore.ru/?post_type=acf-field&#038;p=37', 1, 'acf-field', '', 0),
(38, 1, '2020-06-23 12:35:04', '2020-06-23 09:35:04', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";i:20;s:10:\"mime_types\";s:3:\"mp3\";}', 'File', 'file', 'publish', 'closed', 'closed', '', 'field_5eea54c245608', '', '', '2020-06-23 13:46:27', '2020-06-23 10:46:27', '', 34, 'http://fino.dqcore.ru/?post_type=acf-field&#038;p=38', 2, 'acf-field', '', 0),
(39, 1, '2020-06-23 14:39:53', '2020-06-23 11:39:53', '', 'Track #7', '', 'publish', 'closed', 'closed', '', 'track-7', '', '', '2020-06-23 14:39:53', '2020-06-23 11:39:53', '', 0, 'http://fino.dqcore.ru/?post_type=tracks&#038;p=39', 0, 'tracks', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Без рубрики', '%d0%b1%d0%b5%d0%b7-%d1%80%d1%83%d0%b1%d1%80%d0%b8%d0%ba%d0%b8', 0),
(2, 'Album', 'album', 0),
(3, 'Collection', 'collection', 0),
(4, 'EP', 'ep', 0),
(5, 'Single', 'single', 0),
(6, 'Remix', 'remix', 0),
(7, 'Main menu', 'main-menu', 0),
(8, 'Album', 'album', 0),
(9, 'Collection', 'collection', 0),
(10, 'EP', 'ep', 0),
(11, 'Single', 'single', 0),
(12, 'Remix', 'remix', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(6, 2, 0),
(7, 3, 0),
(9, 8, 0),
(13, 7, 0),
(17, 11, 0),
(18, 2, 0),
(19, 3, 0),
(20, 6, 0),
(21, 6, 0),
(22, 9, 0),
(23, 10, 0),
(24, 9, 0),
(24, 10, 0),
(27, 1, 0),
(34, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'tax_tracks', '', 0, 2),
(3, 3, 'tax_tracks', '', 0, 2),
(4, 4, 'tax_tracks', '', 0, 0),
(5, 5, 'tax_tracks', '', 0, 0),
(6, 6, 'tax_tracks', '', 0, 2),
(7, 7, 'nav_menu', '', 0, 1),
(8, 8, 'tax_releases', '', 0, 1),
(9, 9, 'tax_releases', '', 0, 2),
(10, 10, 'tax_releases', '', 0, 2),
(11, 11, 'tax_releases', '', 0, 1),
(12, 12, 'tax_releases', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'DSS'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_user-settings', 'libraryContent=browse'),
(18, 1, 'wp_user-settings-time', '1592748833'),
(19, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(20, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:10:\"46.62.95.0\";}'),
(21, 2, 'nickname', 'Admin'),
(22, 2, 'first_name', ''),
(23, 2, 'last_name', ''),
(24, 2, 'description', ''),
(25, 2, 'rich_editing', 'true'),
(26, 2, 'syntax_highlighting', 'true'),
(27, 2, 'comment_shortcuts', 'false'),
(28, 2, 'admin_color', 'fresh'),
(29, 2, 'use_ssl', '0'),
(30, 2, 'show_admin_bar_front', 'true'),
(31, 2, 'locale', ''),
(32, 2, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(33, 2, 'wp_user_level', '10'),
(34, 2, 'dismissed_wp_pointers', ''),
(35, 3, 'nickname', 'Author'),
(36, 3, 'first_name', ''),
(37, 3, 'last_name', ''),
(38, 3, 'description', 'Draw fond rank form nor the day eat. Up hung mr we give rest half. An stairs as be lovers uneasy. Their saved linen downs tears son add music. We leaf to snug on no need. Words to up style of since world. Hard do me sigh with west same lady. Effect if in up no depend seemed.'),
(39, 3, 'rich_editing', 'true'),
(40, 3, 'syntax_highlighting', 'true'),
(41, 3, 'comment_shortcuts', 'false'),
(42, 3, 'admin_color', 'fresh'),
(43, 3, 'use_ssl', '0'),
(44, 3, 'show_admin_bar_front', 'true'),
(45, 3, 'locale', ''),
(46, 3, 'wp_capabilities', 'a:1:{s:6:\"author\";b:1;}'),
(47, 3, 'wp_user_level', '2'),
(48, 3, 'dismissed_wp_pointers', ''),
(49, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(50, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:22:\"add-post-type-releases\";i:1;s:20:\"add-post-type-tracks\";i:2;s:12:\"add-post_tag\";i:3;s:16:\"add-tax_releases\";i:4;s:14:\"add-tax_tracks\";}'),
(53, 3, 'wp_dashboard_quick_press_last_post_id', '16'),
(54, 3, 'community-events-location', 'a:1:{s:2:\"ip\";s:10:\"46.62.95.0\";}'),
(55, 3, 'wp_user-settings', 'libraryContent=browse'),
(56, 3, 'wp_user-settings-time', '1592749656'),
(57, 3, 'wp_user_avatar', '11'),
(59, 1, 'nav_menu_recently_edited', '7'),
(62, 2, 'session_tokens', 'a:1:{s:64:\"82594149b94494fc567cde7fd67d96c52b7059fb747e26a2d614201c5a3018eb\";a:4:{s:10:\"expiration\";i:1594037147;s:2:\"ip\";s:15:\"163.172.217.200\";s:2:\"ua\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0\";s:5:\"login\";i:1592827547;}}'),
(63, 2, 'wp_dashboard_quick_press_last_post_id', '25'),
(64, 2, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"163.172.217.0\";}'),
(65, 2, 'closedpostboxes_releases', 'a:0:{}'),
(66, 2, 'metaboxhidden_releases', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(68, 1, 'session_tokens', 'a:1:{s:64:\"9828ac508d665bbb29a93bf3a73d7f3250b0ded9fa4ac900b8b567e1f46d7e12\";a:4:{s:10:\"expiration\";i:1593093872;s:2:\"ip\";s:11:\"46.62.95.24\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36\";s:5:\"login\";i:1592921072;}}');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'DSS', '$P$BcxF/eL6d/9N24L/kMGO2Af2EMcEgr0', 'dss', 'gajver1809@mail.ru', 'http://fino.dqcore.ru', '2020-06-21 14:13:51', '', 0, 'DSS'),
(2, 'Admin', '$P$B9DBSCY3tKds0LMxMaAwa5h6oqSwd8.', 'admin', 'admin@dqcore.ru', '', '2020-06-21 14:17:12', '', 0, 'Admin'),
(3, 'Author', '$P$B2bxtT5AFYbnqm4wPo4ikJXiL2MGPO0', 'author', 'author@dqcore.ru', '', '2020-06-21 14:18:34', '', 0, 'Author');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Индексы таблицы `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Индексы таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Индексы таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Индексы таблицы `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=263;

--
-- AUTO_INCREMENT для таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT для таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT для таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT для таблицы `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
