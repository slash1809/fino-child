<?php get_header();
if( ! empty( $_GET['user'] ) ) :
    $user = new WP_User((int)$_GET['user']);
    ?>
    <section id="artist">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <?php
                    $avatar = get_avatar($user->ID, '150');
                    if($avatar): ?>
                        <?= $avatar ?>
                    <?php else : ?>
                        <img class="avatar" src="<?= get_stylesheet_directory_uri() . '/assets/img/no-avatar.jpg' ?>" alt="" width="150" height="150">
                    <?php endif; ?>
                </div>
                <div class="col-md-10">
                    <div class="row main_information">
                        <div class="col-md-12">
                            <h1><?= $user->display_name ?></h1>
                        </div>
                        <?php if( !empty( $user->get('description') ) ): ?>
                        <div class="col-md-12">
                            <h3>About me:</h3>
                            <div class="description"><?= $user->get('description') ?></div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <h3>Releases</h3>
                    <div class="releases_container">
                        <?php
                        $terms = getDiscography();
                        if( ! empty($terms)):
                            foreach($terms as $term): ?>
                                <h4><?= $term['name'] ?></h4>
                                <?php if($term['posts']):
                                    foreach($term['posts'] as $term_posts): ?>
                                        <div class="release">
                                            <div><?= $term_posts->ID ?></div>
                                            <div><a href="<?= get_the_permalink( $term_posts->ID ) ?>"><?= $term_posts->post_title ?></a></div>
                                            <div><? ?></div>
                                        </div>
                                    <?php endforeach;
                                endif; ?>
                            <?php endforeach;
                        endif; ?>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Tracks list</h3>
                    <div class="tracks_container">
                        <?php
                        $data = getTrackListWithDate();
                        if( ! empty($data)):
                            foreach( $data as $key => $tracks ):
                                foreach( $tracks as $track_id ) : $track = get_post( $track_id ) ?>
                                <div class="track">
                                    <div><?= $track->ID ?></div>
                                    <div><?= $track->post_title ?></div>
                                    <div><?= $key ?></div>
                                </div>
                            <?php endforeach;
                            endforeach;
                        endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php else : ?>
    <section id="artist">
        <div class="container">
            <div class="row">
                <div class="col-md-12 artist_not_found">
                    <h2>Artist not found!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php get_footer(); ?>
