jQuery('document').ready( function( $ ){
	$('#order_by').change( function(){
		let $sort = $( this ).val();
		if( $.query.has('order_by' ) ){
			window.location.href = $.query.set('order_by', $sort ).toString();
		}
	});

	$('#select_per_page').change( function(){
		let $limit = $( this ).val();
		if( $.query.has('limit' ) ){
			window.location.href = $.query.set('limit', $limit ).toString();
		}
	});

	$('.previous_watched').owlCarousel();
});