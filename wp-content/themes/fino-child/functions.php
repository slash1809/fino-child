<?php

define('FINO_VER', '0.0.1');
//Releases
define('FINO_TYPE_RELEASE', 'releases');
define('FINO_RELEASE_TAX', 'tax_releases');
//Tracks
define('FINO_TYPE_TRACKS', 'tracks');
//Filters
define('FINO_FILTER_PREFIX', 'filter_');

//Sets post type and taxonomy
require 'include/post-types.php';
//Sets track lists and releases functionality
require 'include/track-list-and-releases.php';
//Sets filters and sorts functionality
require 'include/filters-and-sort.php';
//Sets filter for releases
require_once 'classes/widgets/FilterReleasesWidget.php';

add_action('init', 'fino_start_session', 1);
function fino_start_session() {
    if(!session_id()) {
        session_start();
    }
}

add_action('wp_logout', 'end_session');
add_action('wp_login', 'end_session');
function end_session() {
    session_destroy ();
}

function filterReleasesWidget() {
    register_widget( 'FilterReleasesWidget' );
}
add_action( 'widgets_init', 'filterReleasesWidget' );

add_image_size( 'fino_release_img', 200, 200, true );

add_action('wp_enqueue_scripts', 'finoScripts');
function finoScripts()
{
    // Add styles.
    wp_enqueue_style('fino-child-style', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), FINO_VER); //main css
    // Add scripts
    wp_enqueue_script('fino-child-query-object-js', get_stylesheet_directory_uri() . '/assets/js/jquery.query-object.js', array(), FINO_VER, true);
    wp_enqueue_script('fino-child-js', get_stylesheet_directory_uri() . '/assets/js/common.js', array(), FINO_VER, true); //main js
}

/**
 * This function sets the release in a session variable
 */
function setReleaseSession()
{
    $release = get_the_ID();
    if( ! empty($_SESSION['previous_watched'])) {
        if(array_search($release, $_SESSION['previous_watched']) === false) {
            $_SESSION['previous_watched'][] = $release;
        }
    } else {
        $_SESSION['previous_watched'][] = $release;
    }
}

/**
 * If we haven't got page "artist", then we creates this page.
 */
if( !get_page_by_title('artist') ){

    $post_data = array(
        'post_title'    => 'artist',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type'     => 'page'
    );

    $post_id = wp_insert_post( $post_data );
}


