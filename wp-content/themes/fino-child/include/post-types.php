<?php
add_action('init', 'finoReleasePostType');
/**
 * This function sets a new post types
 */
function finoReleasePostType()
{
    register_post_type(FINO_TYPE_RELEASE, array(
        'labels'             => array(
            'name'               => 'Releases',
            'singular_name'      => 'Release',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Release',
            'edit_item'          => 'Edit Release',
            'new_item'           => 'New Release',
            'view_item'          => 'View Release',
            'search_items'       => 'Find Release',
            'not_found'          => 'Release Not Found!',
            'not_found_in_trash' => 'No release found in the basket!',
            'parent_item_colon'  => '',
            'menu_name'          => 'Releases'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title', 'editor', 'thumbnail')
    ));
}

add_action('init', 'finoTracksPostType');
/**
 * This function sets a new post type for the tracks
 */
function finoTracksPostType()
{
    register_post_type(FINO_TYPE_TRACKS, array(
        'labels'             => array(
            'name'               => 'Tracks',
            'singular_name'      => 'Track',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Track',
            'edit_item'          => 'Edit Track',
            'new_item'           => 'New Track',
            'view_item'          => 'View Track',
            'search_items'       => 'Find Track',
            'not_found'          => 'Track Not Found!',
            'not_found_in_trash' => 'No track found in the basket!',
            'parent_item_colon'  => '',
            'menu_name'          => 'Tracks'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title', 'editor', 'thumbnail')
    ));
}

add_action('init', 'finoReleasesTax');
/**
 * This function sets a new taxonomy for the releases
 */
function finoReleasesTax()
{
    register_taxonomy(FINO_RELEASE_TAX, [FINO_TYPE_RELEASE], [
        'label'        => '',
        'labels'       => [
            'name'              => 'Formats',
            'singular_name'     => 'Format',
            'search_items'      => 'Search Format',
            'all_items'         => 'All Formats',
            'view_item '        => 'View Format',
            'parent_item'       => 'Parent Format',
            'parent_item_colon' => 'Parent Format:',
            'edit_item'         => 'Edit Format',
            'update_item'       => 'Update Format',
            'add_new_item'      => 'Add New Format',
            'new_item_name'     => 'New Format Name',
            'menu_name'         => 'Formats',
        ],
        'description'  => '',
        'public'       => true,
        'hierarchical' => true
    ]);
}
