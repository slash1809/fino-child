<?php
add_action('add_meta_boxes', 'setTrackListForRelease', 1);
/**
 * This function sets a custom field "Tracks list" for the releases
 */
function setTrackListForRelease()
{
    add_meta_box('track_list', 'Tracks list', 'createFieldForTrackList', FINO_TYPE_RELEASE, 'advanced', 'high');
}

/**
 * This function creates a field html for custom field "Tracks list"
 *
 * @param $post
 *
 * @return bool
 */
function createFieldForTrackList($post)
{
    $user_id = get_current_user_id();
    if(empty($user_id)) {
        return false;
    }

    $args = array(
        'numberposts' => -1,
        'post_type'   => FINO_TYPE_TRACKS
    );

    if( ! current_user_can('delete_pages')) {
        $args['author'] = (int)$user_id;
    }

    $tracks = get_posts($args);
    if( ! empty($tracks)): ?>
        <select name="tracks_list[]" style="width: 100%" size="15" multiple>
            <?php foreach($tracks as $track) :
                setup_postdata($track);
                $selected = hasTrackSelected($post->ID, $track->ID);
                ?>
                <option value="<?= $track->ID ?>"<?php if($selected): ?> selected<?php endif; ?>><?= $track->post_title ?></option>
            <?php endforeach; ?>
        </select>
        <?php
        wp_nonce_field('tracks_list_nonce_action', 'tracks_list_nonce');
        unset($track);
    else : ?>
        <h3>You don't have tracks for release!</h3>
    <?php endif;
}

add_action('save_post', 'saveTrackList');
/**
 * This function saves a data for the field "Tracks list"
 *
 * @param $post_id
 */
function saveTrackList($post_id)
{
    if( ! isset($_POST['tracks_list'])) {
        return;
    }

    if( ! wp_verify_nonce($_POST['tracks_list_nonce'], 'tracks_list_nonce_action')) {
        return;
    }

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if( ! current_user_can('edit_post', $post_id)) {
        return;
    }

    $tracks_list = serialize($_POST['tracks_list']);

    update_post_meta($post_id, 'tracks_list', $tracks_list);

    return $post_id;
}

/**
 * This function checks which item "Track List" has been selected.
 *
 * @param $post_id
 * @param $track_id
 *
 * @return bool|false|int|string
 */
function hasTrackSelected($post_id, $track_id)
{
    $selected_list = get_post_meta($post_id, 'tracks_list', true);
    if( ! empty($selected_list)) {
        $selected_list = unserialize($selected_list);
        $result        = array_search($track_id, $selected_list);
        if($result === false) {
            return false;
        } else {
            return true;
        }
    }

    return false;
}

/**
 * This function gets a string from release formats.
 *
 * @param $post_id
 *
 * @return string
 */
function getFormatsForRelease($post_id)
{
    $str   = '';
    $terms = get_the_terms($post_id, FINO_RELEASE_TAX);

    if( ! empty($terms)) {
        foreach($terms as $key => $term) {
            if(array_key_last($terms) == $key) {
                $str .= $term->name;
            } else {
                $str .= $term->name . ', ';
            }
        }
    }

    return $str;
}

/**
 * This function gets all users, who worked on the release.
 *
 * @param array $users
 *
 * @return bool|string
 */
function getWorkedOnRelease($users)
{
    $str  = '';
    $link = get_home_url() . '/artist?user=';

    if( ! empty($users) && is_array($users)) {
        if(is_wp_error($users[0])) {
            return false;
        }
        foreach($users as $key => $user) {
            if(array_key_last($users) == $key) {
                $str .= '<a href="' . $link . $user['ID'] . '">' . $user['display_name'] . '</a>';
            } else {
                $str .= '<a href="' . $link . $user['ID'] . '">' . $user['display_name'] . '</a>, ';
            }
        }
    }

    return $str;
}

/**
 * This function gets a releases list separation by formats on the artist page.
 * @return array|bool
 */
function getDiscography()
{
    $result  = array();
    $user_id = ! empty($_GET['user']) ? (int)$_GET['user'] : __return_zero();

    $args = array(
        'numberposts' => -1,
        'author'      => $user_id,
        'post_type'   => FINO_TYPE_RELEASE
    );

    $terms = get_terms(array(
        'taxonomy'   => FINO_RELEASE_TAX,
        'hide_empty' => true
    ));

    if( ! empty($terms)) {
        foreach($terms as $key => $term) {

            $args['tax_query'] = array(
                array(
                    'taxonomy' => FINO_RELEASE_TAX,
                    'field'    => 'term_id',
                    'terms'    => $term->term_id
                )
            );

            $result[$key]['id']    = $term->term_id;
            $result[$key]['name']  = $term->name;
            $result[$key]['posts'] = get_posts($args);

            //If not have posts in the term, then we delete this term
            if(empty($result[$key]['posts'])) {
                unset($result[$key]);
            }
        }

        return $result;
    }

    return false;
}

/**
 * This function gets a tracks list with date on the artist page.
 * @return array
 */
function getTrackListWithDate()
{
    $result  = array();
    $user_id = ! empty($_GET['user']) ? (int)$_GET['user'] : __return_zero();

    $releases = get_posts(array(
        'numberposts' => -1,
        'author'      => $user_id,
        'post_type'   => FINO_TYPE_RELEASE
    ));

    if( ! empty($releases)) {
        foreach($releases as $release) {
            $posts = get_post_meta($release->ID, 'tracks_list', true);
            if($posts) {
                $release_date = get_field('release_date', $release->ID );
                $posts        = unserialize($posts);
                foreach( $posts as $post ) {
                    $result[$release_date][] = $post;
                }
                $result[$release_date] = array_unique( $result[$release_date] );
            }
        }
    }

    return $result;

}


/**
 *  This function creates new short code for slider "previous_watched"
 */
add_shortcode('previous_watched', function($atts){
    $watched         = $_SESSION['previous_watched'];
    if( ! empty($watched)) :
        $current_release = $atts['current_id'];
        $current_key = array_search($current_release, $watched);
        if($current_key !== false) {
            unset($watched[$current_key]);
        }
        //If we have no posts other than the current one, then return.
        if(empty($watched)) {
            return;
        }
        ?>
        <h3 style="text-align: center">Previous watched:</h3>
        <div class="previous_watched owl-carousel owl-theme">
            <?php foreach($watched as $release_id) :
                $post = get_post($release_id);
                if(empty($post)) {
                    continue;
                } ?>
                <div class="slide_release">
                    <div>
                        <a href="<?= $post->guid ?>">
                            <span><?= $post->post_title ?></span>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif;
});
