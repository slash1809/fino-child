<?php
/**
 *  This function gets HTML for the filter "Sort by" on the releases page. .
 */
function getOrderByHtml()
{
    $sorts = array(
        'date'         => 'By date',
        'title'        => 'By title',
        'release_date' => 'By release date'
    );
    ?>
    <select name="order_by" id="order_by">
        <?php foreach($sorts as $value => $title) :
            $selected = ! empty($_GET['order_by']) && $_GET['order_by'] == $value ? ' selected' : '' ?>
            <option value="<?= $value ?>"<?= $selected ?>><?= $title ?></option>
        <?php endforeach; ?>
    </select>
    <?php
}

/**
 *  This function gets HTML for the filter "Posts per page" on the releases page. .
 */
function getLimitByHtml()
{
    $limit = array(
        50,
        100,
        200
    );
    ?>
    <label for="select_per_page">Posts per pages</label>
    <select id="select_per_page" name="select_per_page">
        <option value="">---</option>
        <?php foreach($limit as $value) :
            $selected = ! empty($_GET['limit']) && $_GET['limit'] == $value ? ' selected' : '' ?>
            <option value="<?= $value ?>"<?= $selected ?>><?= $value ?></option>
        <?php endforeach; ?>
    </select>
    <?php
}

/**
 * This function changes a data array for the sort.
 *
 * @param array $args
 */
function createSortData(&$args)
{
    $order_by = ! empty($_GET['order_by']) ? htmlspecialchars($_GET['order_by']) : 'date';

    if($order_by == 'date' or $_GET['order_by'] == 'date' or $_GET['order_by'] == 'title') {
        $args['orderby'] = $order_by;
    } else {
        $args['meta_key'] = $order_by;
        $args['orderby']  = 'meta_value';
    }

}

function createFilterData(&$args)
{
    $result = array();
    //Deletes filter for terms
    if( isset( $_GET[FINO_FILTER_PREFIX.'format'] )  ) unset($_GET[FINO_FILTER_PREFIX.'format']);

    if( ! empty($_GET)) {
        foreach($_GET as $key => $item) {
            $pattern = '/^' . FINO_FILTER_PREFIX . '/';
            if(preg_match($pattern, $key)) {
                $filter                  = preg_replace($pattern, '', $key);
                $result[$key]['key']     = htmlspecialchars(trim($filter));
                $result[$key]['value']   = htmlspecialchars(trim($item));
                $result[$key]['compare'] = 'LIKE';
            }
        }
    }

    $args['meta_query'] = array_values($result);
}


