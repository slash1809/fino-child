<?php
setReleaseSession();
get_header();
if(have_posts()): the_post();
    $fields  = get_fields();
    $user_id = get_the_author_meta('ID');
    ?>
    <section id="single_release">
        <div class="container">
            <!-- Main information -->
            <div class="row">
                <div class="col-md-3" style="padding: 10px">
                    <?php
                    $image_link = get_the_post_thumbnail_url('', 'fino_release_img');
                    if( empty( $image_link ) ) $image_link = get_stylesheet_directory_uri().'/assets/img/not-found.png'; ?>
                    <img src="<?= $image_link ?>" alt="">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>
                                <a href="<?= home_url('/artist?user=') . $user_id ?>"><? the_author() ?></a>
                            </h2>
                            <span>&#8212;</span>
                            <h1><?php the_title() ?></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 information_container">
                            <div class="row">
                                <div class="col-md-2 heads">Genre:</div>
                                <div class="col-md-10"><?= ! empty($fields['genre']) ? implode(', ', $fields['genre']) : '' ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 heads">Style:</div>
                                <div class="col-md-10"><?= ! empty($fields['style']) ? implode(', ', $fields['style']) : '' ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 heads">Format:</div>
                                <div class="col-md-10"><?= getFormatsForRelease( $post->ID ) ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 heads">Country:</div>
                                <div class="col-md-10"><?= $fields['country'] ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 heads">Release date:</div>
                                <div class="col-md-10"><?= $fields['release_date'] ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 heads">Worked on the release:</div>
                                <div class="col-md-10"><?= getWorkedOnRelease($fields['worked_on_release']) ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 heads">Description:</div>
                                <div class="col-md-10"><? the_content(); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TracksList -->
            <div class="row">
                <div class="col-md-12">
                    <table id="track_list">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>TITLE</td>
                            <td>TIME</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $tracks = get_post_meta( get_the_ID(), 'tracks_list', true );
                        if($tracks):
                            $tracks = unserialize($tracks);
                            foreach($tracks as $track_id ):
                                $track = get_post($track_id);
                                $time  = get_field('time',$track_id);

                                ?>
                                <tr>
                                    <td><?= $track->ID ?></td>
                                    <td><?= $track->post_title ?></td>
                                    <td><?= $time ?></td>
                                </tr>
                            <?php endforeach;
                            else :?>
                                <tr>
                                    <td colspan="3">Tracks list is empty!</td>
                                </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= do_shortcode('[previous_watched current_id="'.$post->ID.'"]') ?>
                </div>
            </div>
        </div>
    </section>
<?php else : ?>
<?php endif; ?>
<?php get_footer(); ?>