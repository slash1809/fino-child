<?php


class FilterReleasesWidget extends WP_Widget
{
    /**
     * ACF group ID for releases fields
     * @var int
     */
    private $acf_group = 0;

    /**
     * All filters
     * @var array
     */
    private $filters = [];

    /**
     * $all_releases contains all posts
     * @var int[]|WP_Post[]
     */
    private $all_releases;

    /**
     * $url contains current array $_GET
     * @var array
     */
    private static $url;

    /**
     * Home page link
     * @var string|void
     */
    private static $home_url;

    /**
     * FilterReleasesWidget constructor.
     */
    function __construct()
    {
        parent::__construct(
            'filter_releases_widget',
            'Releases filter',
            array('description' => '')
        );

        self::$url          = $_GET;
        self::$home_url     = home_url('/');
        $this->all_releases = get_posts(array(
            'numberposts'      => -1,
            'post_type'        => FINO_TYPE_RELEASE,
            'suppress_filters' => true
        ));
    }

    /**
     * This is frontend
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        $title           = apply_filters('widget_title', $instance['title']);
        $this->acf_group = (int)$instance['acf_releases_group'];

        echo $args['before_widget'];

        if( ! empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        $this->setFilters();
        $this->getWidgetHtml();

        echo $args['after_widget'];
    }

    /**
     * This is backend
     *
     * @param array $instance
     *
     * @return string|void
     */
    public function form($instance)
    {
        if(isset($instance['title'])) {
            $title = $instance['title'];
        }
        if(isset($instance['acf_releases_group'])) {
            $acf_releases_group = $instance['acf_releases_group'];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Заголовок</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('acf_releases_group'); ?>">Releases group ID in ACF</label>
            <input class="widefat" id="<?php echo $this->get_field_id('acf_releases_group'); ?>" name="<?php echo $this->get_field_name('acf_releases_group'); ?>" type="text"
                   value="<?php echo esc_attr($acf_releases_group); ?>"/>
        </p>
        <?php
    }

    /**
     * This function saves data of widget
     *
     * @param array $new_instance
     * @param array $old_instance
     *
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance                       = array();
        $instance['title']              = ( ! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['acf_releases_group'] = (is_numeric($new_instance['acf_releases_group'])) ? $new_instance['acf_releases_group'] : 0;

        return $instance;
    }

    /**
     * This function sets filters services
     */
    private function setFilters()
    {
        $this->setFormats();
        $this->setCustomFields();
    }

    /**
     * This function gets count releases for filter item
     *
     * @param string $meta_key
     * @param string $value
     * @param bool   $single
     *
     * @return bool|int
     */
    private function getCount($meta_key = '', $value = '', $single = false)
    {
        if(empty($meta_key)) {
            return false;
        }

        global $wpdb;
        $count    = 0;
        $meta_key = esc_sql($meta_key);
        $value    = esc_sql($value);
        $table    = $wpdb->prefix . 'postmeta';
        $posts    = $wpdb->get_results("SELECT `post_id`, `meta_value` FROM `{$table}` WHERE `meta_key`='{$meta_key}';");

        if(empty($posts)) {
            return false;
        }

        if($single) {
            foreach($posts as $post) {
                $meta_value = $meta_key == 'release_date' ? date('Y', strtotime($post->meta_value)) : $post->meta_value;
                if($meta_value == trim($value)) {
                    $count += 1;
                }
            }
        } else {
            foreach($posts as $post) {
                $data = unserialize($post->meta_value);
                if(in_array($value, $data)) {
                    $count += 1;
                }
            }
        }


        return $count;
    }

    /**
     * Filter formats
     */
    private function setFormats()
    {
        $id = get_the_ID();
        echo get_the_permalink($id);

        $data  = array();
        $terms = get_terms(array(
            'taxonomy'   => FINO_RELEASE_TAX,
            'hide_empty' => true,
            'count'      => true
        ));

        if(empty($terms)) {
            return;
        }

        $data['title'] = 'Formats';
        $data['slug']  = 'format';
        $data['close'] = self::closeFilterLink($data['slug']);

        foreach($terms as $term) {
            $data['links'][] = array(
                'title' => $term->name,
                'value' => $term->slug,
                'count' => $term->count,
                'link'  => self::buildingLinks($data['slug'], $term->slug)
            );
        }

        array_push($this->filters, $data);

    }

    /**
     * Filter all custom fields for ACF
     * @return bool
     */
    private function setCustomFields()
    {
        if(empty($this->acf_group)) {
            return false;
        }

        $filters = acf_get_fields($this->acf_group);
        //deletes field "Worked on the release"
        $last = array_key_last($filters);
        unset($filters[$last]);

        if( ! empty($filters)) {
            foreach($filters as $filter) {
                $data          = array();
                $data['title'] = $filter['label'];
                $data['slug']  = $filter['name'];
                $data['close'] = self::closeFilterLink($data['slug']);
                $data['links'] = array();

                if( ! empty($filter['choices'])) {
                    foreach($filter['choices'] as $slug => $label) {
                        $data['links'][] = array(
                            'title' => $label,
                            'value' => $slug,
                            'count' => $this->getCount($data['slug'], $slug),
                            'link'  => self::buildingLinks($data['slug'], $slug)
                        );
                    }
                } else {
                    $this->setTextFilter($data);
                }
                array_push($this->filters, $data);
            }

        }

        return false;
    }

    /**
     * This function looking for all data for filter with type is string, and sets filter
     *
     * @param $data
     */
    function setTextFilter(&$data)
    {
        $result = array();
        if( ! empty($this->all_releases)) {

            foreach($this->all_releases as $release) {
                $field    = get_field($data['slug'], $release->ID);
                $result[] = $field;
            }

            $result = array_unique($result);

            foreach($result as $item) {
                $data['links'][] = array(
                    'title' => $item,
                    'value' => $item,
                    'count' => $this->getCount($data['slug'], $item, true),
                    'link'  => self::buildingLinks($data['slug'], $item)
                );
            }

        }
    }

    /**
     * This function creates current link for item of filters
     *
     * @param $filter
     * @param $value
     *
     * @return string
     */
    private static function buildingLinks($filter, $value)
    {
        $link               = self::$url;
        $filter_name        = FINO_FILTER_PREFIX . $filter;
        $link[$filter_name] = $value;

        return self::$home_url . '?' . http_build_query($link);
    }

    /**
     * This function gets link for close button of filter
     *
     * @param $item
     *
     * @return array|string
     */
    private static function closeFilterLink($item)
    {
        $link = self::$url;
        $item = FINO_FILTER_PREFIX . $item;

        if(array_key_exists($item, $link)) {
            unset($link[$item]);

            if(empty($link)) {
                return self::$home_url;
            }

            return self::$home_url . '?' . http_build_query($link);
        } else {
            return $link;
        }
    }

    /**
     * This function shows HTML for all filters
     */
    private function getWidgetHtml()
    {
        if(empty($this->filters)) {
            return;
        }
        foreach($this->filters as $field): ?>
            <div id="releases_filter">
                <div class="filter_cont">
                    <h4><?= $field['title'] ?></h4>
                    <ul>
                        <?php if( ! empty($field['links'])) : foreach($field['links'] as $link): ?>
                            <li class="filter_data">
                                <a class="title" href="<?= $link['link'] ?>"><?= $link['title'] ?></a>
                                <div>
                                    <span><?= $link['count'] ?></span>
                                    <?php
                                    $parameter = FINO_FILTER_PREFIX . trim($field['slug']);
                                    if( ! empty($_GET[$parameter]) && $_GET[$parameter] == $link['value']): ?>
                                        <a href="<?= $field['close'] ?>" class="fa fa-window-close"></a>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endforeach;
                        endif; ?>
                    </ul>
                </div>
            </div>
        <?php endforeach;
    }


}
