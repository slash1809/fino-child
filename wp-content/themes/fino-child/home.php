<?php get_header(); ?>
<section id="music">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <?php getOrderByHtml() ?>
                    </div>
                    <div class="col-md-6">
                        <div class="per_page_cont">
                            <? getLimitByHtml(); ?>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 music_container">
                        <?php
                        $paged = ! empty($_GET['page']) ? (int)$_GET['page'] : 1;

                        $args = array(
                            'posts_per_page' => !empty( $_GET['limit'] ) ? (int)$_GET['limit'] : 3,
                            'post_type'      => FINO_TYPE_RELEASE,
                            'paged'          => $paged,
                            'order'          => 'DESC'
                        );
                        //Set term
                        if( !empty( $_GET[FINO_FILTER_PREFIX.'format'] ))
                            $args[FINO_RELEASE_TAX] = $_GET[FINO_FILTER_PREFIX.'format'];
                        //Set order by
                        createSortData($args );
                        //Set filters
                        createFilterData( $args );

                        $query = new WP_Query($args);

                        if ($query->have_posts()):
                            while ($query->have_posts()) : $query->the_post();
                                //Get all custom fields
                                $fields = get_fields();
                                $user_id = get_the_author_meta('ID'); ?>
                                <div class="releases_container">
                                    <div class="release">
                                        <a href="<?php the_permalink(); ?>" class="release_img">
                                            <?php
                                            $image_link = get_the_post_thumbnail_url('', 'fino_release_img' );
                                            if( empty( $image_link ) ) $image_link = get_stylesheet_directory_uri().'/assets/img/not-found.png';
                                            ?>
                                            <img src="<?= $image_link ?>" alt="">
                                        </a>
                                        <a href="<?php the_permalink(); ?>" class="title"><?php the_title() ?></a><br/>
                                        <a href="<?= home_url('/artist?user=').$user_id ?>" class="title"><? the_author() ?></a>
                                    </div>
                                </div>
                            <?php endwhile;
                            else: ?>
                                <div class="release_not_found">
                                    <span>Releases not found!<span>
                                </div>
                        <?php endif;
                        wp_reset_postdata(); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pagination">
                                <?php
                                echo paginate_links(array(
                                    'format'  => '?page=%#%',
                                    'current' => max(1, get_query_var('page')),
                                    'total'   => $query->max_num_pages,
                                    'prev_text' => '&#171;',
                                    'next_text' => '&#187;'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php get_footer(); ?>
